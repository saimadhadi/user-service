package com.epam.userservice.repository;

import com.epam.userservice.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("com.epam.userservice.repository.UserRepository")
public interface UserRepository extends JpaRepository<UserEntity,Long> {
}
