package com.epam.userservice.model;

import com.epam.userservice.entity.UserEntity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class UserModel {

	private Long userId;
	private String userName;
	private String emailId;

	public UserModel(UserEntity userEntity) {
		this.userId = userEntity.getUserId();
		this.userName = userEntity.getUserName();
		this.emailId = userEntity.getEmailId();
	}
}
