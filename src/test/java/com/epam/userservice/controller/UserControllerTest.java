package com.epam.userservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.userservice.entity.UserEntity;
import com.epam.userservice.exception.DataNotFoundException;
import com.epam.userservice.model.UserModel;
import com.epam.userservice.service.impl.UserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private UserServiceImpl userService;

	@InjectMocks
	private UserController userController;

	public static Long userId = 1L;
	public static String userName = "abc";
	public static String userEmail = "abc@gmail.com";
	public static UserEntity userEntity;
	public static UserModel userModel;
	public static ObjectMapper mapper;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	@BeforeAll
	public static void init_data() {
		mapper = new ObjectMapper();
		userModel = new UserModel(userId, userName, userEmail);
	}

	@Test
	public void getAllUsersReturnsUserListAndStatusOk() throws Exception {
		List<UserModel> userModelList = new ArrayList<>();
		userModelList.add(userModel);
		Mockito.when(userService.getAllUsers()).thenReturn(userModelList);
		this.mockMvc.perform(get("/user")).andExpect(content().json(mapper.writeValueAsString(userModelList)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(userService).getAllUsers();

	}

	@Test
	public void getUserByIdReturnsUserAndStatusOk() throws Exception {
		Mockito.when(userService.getUserById(userId)).thenReturn(userModel);
		this.mockMvc.perform(get("/user/1")).andExpect(content().json(mapper.writeValueAsString(userModel)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(userService).getUserById(userId);

	}

	@Test
	public void getUserByIdReturnsStatusNotFound() throws Exception {
		Mockito.when(userService.getUserById(userId)).thenThrow(DataNotFoundException.class);
		this.mockMvc.perform(get("/user/1")).andExpect(status().is4xxClientError());
		Mockito.verify(userService).getUserById(userId);
	}

	@Test
	public void addUserReturnsAddedUserAndStatusCreated() throws Exception {
		Mockito.when(userService.addUser(userModel)).thenReturn(userModel);
		this.mockMvc
				.perform(post("/user").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(userModel)))
				.andExpect(content().json(mapper.writeValueAsString(userModel))).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(userService).addUser(userModel);
	}

	@Test
	public void deleteUserByIdReturnsUserAndStatusOk() throws Exception {
		Mockito.when(userService.deleteUserById(userId)).thenReturn(userModel);
		this.mockMvc.perform(delete("/user/1")).andExpect(content().json(mapper.writeValueAsString(userModel)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(userService).deleteUserById(userId);

	}

	@Test
	public void deleteUserByIdReturnsStatusNotFound() throws Exception {
		Mockito.when(userService.deleteUserById(userId)).thenThrow(DataNotFoundException.class);
		this.mockMvc.perform(delete("/user/1")).andExpect(status().is4xxClientError());
		Mockito.verify(userService).deleteUserById(userId);
	}

	@Test
	public void updateUserReturnsUserAndStatusOk() throws Exception {
		Mockito.when(userService.updateUser(userModel)).thenReturn(userModel);
		this.mockMvc
				.perform(put("/user/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(userModel)))
				.andExpect(content().json(mapper.writeValueAsString(userModel))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(userService).updateUser(userModel);
	}

}
