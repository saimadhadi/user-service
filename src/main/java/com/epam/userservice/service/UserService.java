package com.epam.userservice.service;

import java.util.List;

import com.epam.userservice.model.UserModel;

public interface UserService {

	List<UserModel> getAllUsers();

	UserModel getUserById(Long userId);

	UserModel addUser(UserModel userModel);

	UserModel deleteUserById(Long userId);

	UserModel updateUser(UserModel userModel);
}
