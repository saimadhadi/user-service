create table if not exists user(
	user_id bigint auto_increment,
	name varchar(255),
	email varchar(255),
	primary key(user_id) 
);