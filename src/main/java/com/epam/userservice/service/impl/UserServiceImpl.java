package com.epam.userservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.userservice.controller.UserController;
import com.epam.userservice.entity.UserEntity;
import com.epam.userservice.exception.DataNotFoundException;
import com.epam.userservice.model.UserModel;
import com.epam.userservice.repository.UserRepository;
import com.epam.userservice.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("com.epam.userservice.service.impl.UserServiceImpl")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	private static final Logger log = LogManager.getLogger(UserController.class);

	private static final ObjectMapper mapper = new ObjectMapper();

	@Override
	public List<UserModel> getAllUsers() {
		List<UserModel> userModelList = userRepository.findAll().stream().map(UserModel::new).collect(toList());
		log.info("Fetched userModeList for all users: {}", userModelList);
		return userModelList;
	}

	@Override
	public UserModel getUserById(Long userId) {
		UserEntity userEntity = userRepository.findById(userId)
				.orElseThrow(() -> new DataNotFoundException("User not found"));
		UserModel userModel = mapper.convertValue(userEntity, UserModel.class);
		log.info("Fetched User : {}", userModel);
		return userModel;
	}

	@Override
	public UserModel addUser(UserModel userModel) {
		UserEntity userEntity = mapper.convertValue(userModel, UserEntity.class);
		userEntity = userRepository.save(userEntity);
		UserModel userModelAdded = mapper.convertValue(userEntity, UserModel.class);
		log.info("User has been added : {}", userModel);
		return userModelAdded;
	}

	@Override
	public UserModel deleteUserById(Long userId) {
		UserEntity userEntity = userRepository.findById(userId)
				.orElseThrow(() -> new DataNotFoundException("User not found"));
		userRepository.delete(userEntity);
		UserModel userModel = mapper.convertValue(userEntity, UserModel.class);
		log.info("User with id : {} has been deleted", userId);
		return userModel;
	}

	@Override
	public UserModel updateUser(UserModel userModel) {
		UserEntity userEntity = mapper.convertValue(userModel, UserEntity.class);
		userEntity = userRepository.save(userEntity);
		UserModel userModelUpdated = mapper.convertValue(userEntity, UserModel.class);
		log.info("User : {} has been updated", userModelUpdated);
		return userModelUpdated;
	}
}
