package com.epam.userservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.epam.userservice.exception.DataNotFoundException;
import com.epam.userservice.model.UserModel;
import com.epam.userservice.service.UserService;

@RestController
public class UserController {

	@Autowired
	@Qualifier("com.epam.userservice.service.impl.UserServiceImpl")
	private UserService userService;

	private static final Logger log = LogManager.getLogger(UserController.class);

	@GetMapping("/user")
	public ResponseEntity<List<UserModel>> getAllUsers() {
		List<UserModel> userModelList = userService.getAllUsers();
		log.info("Fetched all users : {}", userModelList);
		return new ResponseEntity<>(userModelList, HttpStatus.OK);
	}

	@GetMapping("/user/{userId}")
	public ResponseEntity<UserModel> getUserById(@PathVariable Long userId) {
		ResponseEntity<UserModel> response;
		try {
			UserModel userModel = userService.getUserById(userId);
			response = new ResponseEntity<>(userModel, HttpStatus.OK);
			log.info("Fetched user by id : {}, {} ", userId, userModel);
		} catch (DataNotFoundException e) {
			log.debug("Exception occured while getting user with id : {}", userId, e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", e);
		}
		return response;
	}

	@PostMapping("/user")
	public ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel) {
		UserModel userModelAdded = userService.addUser(userModel);
		log.info("Added user : {}", userModelAdded);
		return new ResponseEntity<>(userModelAdded, HttpStatus.CREATED);
	}

	@DeleteMapping("/user/{userId}")
	public ResponseEntity<UserModel> deleteUser(@PathVariable Long userId) {
		ResponseEntity<UserModel> response;
		try {
			UserModel userModel = userService.deleteUserById(userId);
			response = new ResponseEntity<>(userModel, HttpStatus.OK);
			log.info("Deleted user with id : {} , {}", userId, userModel);
		} catch (DataNotFoundException e) {
			log.debug("Exception occured while deleting user with id : {}", userId, e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found", e);
		}
		return response;
	}

	@PutMapping("/user/{userId}")
	public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userModel) {
		UserModel userModelUpdated = userService.updateUser(userModel);
		log.info("Updated user : {} to : {}", userModel, userModelUpdated);
		return new ResponseEntity<>(userModelUpdated, HttpStatus.OK);
	}

}
