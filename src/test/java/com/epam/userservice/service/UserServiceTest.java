package com.epam.userservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.userservice.entity.UserEntity;
import com.epam.userservice.exception.DataNotFoundException;
import com.epam.userservice.model.UserModel;
import com.epam.userservice.repository.UserRepository;
import com.epam.userservice.service.impl.UserServiceImpl;

@ExtendWith(SpringExtension.class)
public class UserServiceTest {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	private UserServiceImpl userServiceImpl;

	public static Long userId = 1L;
	public static String userName = "abc";
	public static String userEmail = "abc@gmail.com";
	public static UserEntity userEntity;
	public static UserModel userModel;

	@BeforeAll
	public static void init() {
		userEntity = new UserEntity(userId, userName, userEmail);
		userModel = new UserModel(userId, userName, userEmail);
	}

	@Test
	public void getAllUsersReturnsUserList() {
		when(userRepository.findAll()).thenReturn(Arrays.asList(userEntity));
		assertEquals(Arrays.asList(userModel), userServiceImpl.getAllUsers());
		verify(userRepository).findAll();
	}

	@Test
	public void getUserByIdReturnsUser() {
		when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(userEntity));
		assertEquals(userModel, userServiceImpl.getUserById(userId));
		verify(userRepository).findById(userId);
	}

	@Test
	public void getUserByIdThrowsDataNotFoundException() {
		when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(null));
		assertThrows(DataNotFoundException.class, () -> userServiceImpl.getUserById(userId));
		verify(userRepository).findById(userId);
	}

	@Test
	public void deleteUserByIdRetrunsDeletedUser() {
		when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(userEntity));
		Mockito.doNothing().when(userRepository).delete(userEntity);
		assertEquals(userModel, userServiceImpl.deleteUserById(userId));
		verify(userRepository).findById(userId);
		verify(userRepository).delete(userEntity);
	}

	@Test
	public void deleteUserByIdThrowsDataNotFoundException() {
		when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(null));
		assertThrows(DataNotFoundException.class, () -> userServiceImpl.deleteUserById(userId));
		verify(userRepository).findById(userId);
	}

	@Test
	public void updateUserReturnsUpdatedUser() {
		String userNameToBeUpdated = "def";
		UserEntity userEntityUpdated = new UserEntity(userId, userNameToBeUpdated, userEmail);
		UserModel userModelUpdated = new UserModel(userId, userNameToBeUpdated, userEmail);
		when(userRepository.save(Mockito.any())).thenReturn(userEntityUpdated);
		assertEquals(userModelUpdated, userServiceImpl.updateUser(userModelUpdated));
		verify(userRepository).save(Mockito.any());
	}

	@Test
	public void addUserReturnsAddedUser() {
		when(userRepository.save(Mockito.any())).thenReturn(userEntity);
		assertEquals(userModel, userServiceImpl.addUser(userModel));
		verify(userRepository).save(Mockito.any());
	}
}
